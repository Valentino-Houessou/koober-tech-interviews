/**
 * Main module of the project
 */

const answer = require('./src/Answer');
const fs = require('fs');

/**
 * Main function
 */
async function main() {
  // path to the input
  const path = './input.json';

  //call to answer module to process the total
  const total = JSON.stringify(await answer(path), null, 2);

  //create output.json file with the total
  fs.writeFile('./output.json', total, 'utf8', (error) => {
    if (error) {
      console.log('An error occured while writing JSON Object to File.');
      return console.log(err);
    }
    console.log('output.json is created.');
  });
}

if (require.main === module) {
  main();
}
