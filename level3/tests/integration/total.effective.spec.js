/**
 * Test suites for Total class
 */

const Total = require('../../src/Total');
const Carts = require('../../src/Carts');
const Articles = require('../../src/Articles');
const DeliveryFees = require('../../src/DeliveryFees');
const Discounts = require('../../src/Discounts');

/**
 * Sample data for loadArticles method
 */
const loadArcticlesData = [
  {
    id: 1,
    name: 'water',
    price: 100,
  },
  {
    id: 2,
    name: 'honey',
    price: 200,
  },
  {
    id: 4,
    name: 'tea',
    price: 1000,
  },
];

/**
 * Sample data for loadCarts method
 */
const loadCartsData = [
  {
    id: 1,
    items: [
      {
        article_id: 1,
        quantity: 6,
      },
      {
        article_id: 2,
        quantity: 2,
      },
      {
        article_id: 4,
        quantity: 1,
      },
    ],
  },
  {
    id: 2,
    items: [
      {
        article_id: 2,
        quantity: 2,
      },
    ],
  },
];

/**
 * Sample data for loadDeliveryFeesData method
 */
const loadDeliveryFeesData = [
  {
    eligible_transaction_volume: {
      min_price: 0,
      max_price: 1500,
    },
    price: 800,
  },
  {
    eligible_transaction_volume: {
      min_price: 1500,
      max_price: null,
    },
    price: 0,
  },
];

/**
 * Sample data for loadDiscounts method
 */
const loadDiscountsData = [
  {
    article_id: 2,
    type: 'amount',
    value: 25,
  },
  {
    article_id: 4,
    type: 'percentage',
    value: 30,
  },
];

/**
 * return example
 */
const returnString = '[{"id":1,"total":1650},{"id":2,"total":1150}]';

/**
 * Test suites for cartsTotal() method
 * - empty carts and articles
 */
describe('process carts total - empty carts and articles', () => {
  const total = new Total();
  const carts = new Carts();
  const articles = new Articles();
  const deliveryFees = new DeliveryFees();
  const discounts = new Discounts();

  total.cartsTotal(articles, carts, deliveryFees, discounts);

  test('empty carts', () => expect(total.carts.length).toBe(0));
});

/**
 * Test suites for cartsTotal() method
 * - Good input input
 */
describe('process carts total - Good input', () => {
  const total = new Total();
  const carts = new Carts();
  const articles = new Articles();
  const deliveryFees = new DeliveryFees();
  const discounts = new Discounts();

  articles.loadArticles(loadArcticlesData);
  carts.loadCarts(loadCartsData);
  deliveryFees.loadDeliveryFees(loadDeliveryFeesData);
  discounts.loadDiscounts(loadDiscountsData);

  total.cartsTotal(articles, carts, deliveryFees, discounts);

  test('not empty carts', () => expect(total.carts.length).toBe(2));
  test('effectively added', () => expect(JSON.stringify(total.carts)).toBe(returnString));
});
