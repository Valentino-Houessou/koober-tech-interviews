/**
 * Test suites for get price method
 */

const Discounts = require('../../src/Discounts');
const Discount = require('../../src/Discount');

/**
 * Sample data for loadDiscounts method
 */
const loadDiscountsData = [
  {
    article_id: 2,
    type: 'amount',
    value: 25,
  },
  {
    article_id: 5,
    type: 'percentage',
    value: 30,
  },
];

/**
 * Tests suite for getDiscount
 */

describe('getDiscount() - Effective', () => {
  const discounts = new Discounts();
  discounts.loadDiscounts(loadDiscountsData);

  test('Discount not found', () => expect(discounts.getDiscount(3)).toBeUndefined());
  test('Discount found', () => expect(discounts.getDiscount(2)).toBeInstanceOf(Discount));
});
