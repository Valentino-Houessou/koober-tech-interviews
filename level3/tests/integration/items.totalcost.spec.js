/**
 * Test suites for Items class
 */

const Items = require('../../src/Items');
const Articles = require('../../src/Articles');
const Discounts = require('../../src/Discounts');

/**
 * Sample data for loadItems method
 */
const loadItemsData = [
  {
    article_id: 1,
    quantity: 6,
  },
  {
    article_id: 2,
    quantity: 2,
  },
  {
    article_id: 4,
    quantity: 1,
  },
];

/**
 * Sample data for loadBadItems method
 */
const loadBadItemsData = [
  {
    article_id: 3,
    quantity: 6,
  },
  {
    article_id: 2,
    quantity: 2,
  },
];

/**
 * Sample data for loadArticles method
 */
const loadArcticlesData = [
  {
    id: 1,
    name: 'water',
    price: 100,
  },
  {
    id: 2,
    name: 'honey',
    price: 200,
  },
  {
    id: 4,
    name: 'tea',
    price: 1000,
  },
];

/**
 * Sample data for loadDiscounts method
 */
const loadDiscountsData = [
  {
    article_id: 2,
    type: 'amount',
    value: 25,
  },
  {
    article_id: 4,
    type: 'percentage',
    value: 30,
  },
];

/**
 * Test suite for itemsTotalCost method
 * - empty items list
 */
describe('items total cost - bad input - empty list', () => {
  try {
    const items = new Items();

    const articles = new Articles();
    const discounts = new Discounts();

    test('empty items list', () => expect(items.itemsTotalCost(articles, discounts)).toBe(0));
  } catch (error) {
    throw `items total cost process failed: Error ${error}`;
  }
});

/**
 * Test suite for itemsTotalCost method
 * - Missing items id in articles
 */
describe('items total cost - effective', () => {
  try {
    const items = new Items();

    const articles = new Articles();
    const discounts = new Discounts();
    articles.loadArticles(loadArcticlesData);
    discounts.loadDiscounts(loadDiscountsData);
    items.loadItems(loadBadItemsData);

    test('Only good items cost', () => expect(items.itemsTotalCost(articles, discounts)).toBe(350));
  } catch (error) {
    throw `items total cost process failed: Error ${error}`;
  }
});

/**
 * Test suite for itemsTotalCost method
 * - Effective
 */
describe('items total cost - effective', () => {
  try {
    const items = new Items();

    const articles = new Articles();
    const discounts = new Discounts();
    articles.loadArticles(loadArcticlesData);
    items.loadItems(loadItemsData);
    discounts.loadDiscounts(loadDiscountsData);

    test('effective cost', () => expect(items.itemsTotalCost(articles, discounts)).toBe(1650));
  } catch (error) {
    throw `items total cost process failed: Error ${error}`;
  }
});
