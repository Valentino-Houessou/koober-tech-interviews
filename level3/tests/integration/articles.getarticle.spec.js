/**
 * Test suites for get price method
 */

const Articles = require('../../src/Articles');
const Article = require('../../src/Article');
/**
 * Sample data for loadArticles method
 */
const loadArcticlesData = [
  {
    id: 1,
    name: 'water',
    price: 100,
  },
  {
    id: 2,
    name: 'honey',
    price: 200,
  },
];

/**
 * Tests suite for getArticle
 */

describe('getArticle() - Effective', () => {
  const articles = new Articles();
  articles.loadArticles(loadArcticlesData);

  test('Article not found', () => expect(articles.getArticle(3)).toBeUndefined());
  test('Article found', () => expect(articles.getArticle(1)).toBeInstanceOf(Article));
});
