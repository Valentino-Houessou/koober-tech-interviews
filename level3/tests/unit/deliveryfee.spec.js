/**
 * Test suites for DeliveryFee class
 */

const DeliveryFee = require('../../src/DeliveryFee');
const EligibleTransactionVolume = require('../../src/EligibleTransactionVolume');

jest.mock('../../src/EligibleTransactionVolume');

beforeEach(() => {
  EligibleTransactionVolume.mockClear();
});

/**
 * Test suite for DeliveryFee constructor
 */

describe('DeliveryFee class', () => {
  describe('constructor', () => {
    test('bad inputs', () => {
      expect(() => new DeliveryFee()).toThrow('bad input');
      expect(() => new DeliveryFee(new Object())).toThrow('bad input');
      expect(() => new DeliveryFee(new Object(), new Object())).toThrow('bad input');
    });
    test('good inputs', () => {
      const eTV = new EligibleTransactionVolume(0, 1000);
      expect(new DeliveryFee(eTV, 800)).toBeInstanceOf(DeliveryFee);
    });
  });
});
