/**
 * Test suites for Discounts class
 */

const Discounts = require('../../src/Discounts');
const Discount = require('../../src/Discount');

jest.mock('../../src/Discount');

beforeEach(() => {
  Discount.mockClear();
});

/**
 * Sample data for loadDiscounts method
 */
const loadDiscountsData = [
  {
    article_id: 2,
    type: 'amount',
    value: 25,
  },
  {
    article_id: 5,
    type: 'percentage',
    value: 30,
  },
];

/**
 * missing property - data for loadDiscount method
 */
const missingPropertyData = [
  {
    article_id: 2,
    value: 25,
  },
];

/**
 * Test suite for discounts
 */
describe('discount class', () => {
  // Test for constructor
  describe('constructor', () => {
    const discounts = new Discounts();

    test('is instantiated', () => expect(discounts).toBeInstanceOf(Discounts));
    test('has list', () => expect(Array.isArray(discounts.list)).toBeTruthy());
  });

  // Test for add
  describe('Add()', () => {
    const discounts = new Discounts();
    const discount = new Discount(2, 'amount', 25);
    const length = discounts.list.length;

    test('No input given', () => expect(() => discounts.add()).toThrow('bad input'));
    test('bad input type', () => expect(() => discounts.add(new Object())).toThrow('bad input'));

    discounts.add(discount);

    test('effectively added', () => expect(discounts.list.length === length + 1).toBeTruthy());
  });

  /**
   * Test suite for loadDiscounts method
   */
  describe('loadDiscounts()', () => {
    const discounts = new Discounts();

    test('No input given', () => expect(() => discounts.loadDiscounts()).toThrow('bad input'));
    test('bad input type', () => expect(() => discounts.loadDiscounts(new Object())).toThrow('bad input'));

    test('bad format', () =>
      expect(() => discounts.loadDiscounts(missingPropertyData)).toThrow(
        'missing required property in discounts data file'
      ));

    discounts.loadDiscounts([]);
    test('handle empty array', () => expect(discounts.list.length).toBe(0));
  });

  describe('loadDiscounts() - effective', () => {
    test('effectively added', () => {
      const discounts = new Discounts();

      const length = discounts.list.length;
      const iLenght = loadDiscountsData.length;

      discounts.loadDiscounts(loadDiscountsData);

      expect(discounts.list.length === length + iLenght).toBeTruthy();
      expect(Discount).toHaveBeenCalledTimes(2);
    });
  });

  /**
   * Tests suite for getDiscount
   */
  describe('getDiscount()', () => {
    describe('get an discount - bad input', () => {
      const discounts = new Discounts();
      test('No input', () => expect(() => discounts.getDiscount()).toThrow('bad input'));
      test('bad input type', () => expect(() => discounts.getDiscount(new Object())).toThrow('bad input'));

      test('not found', () => expect(discounts.getDiscount(1)).toBeUndefined());
    });
  });
});
