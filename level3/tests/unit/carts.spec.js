/**
 * Test suites for Carts class
 */

const Carts = require('../../src/Carts');
const Cart = require('../../src/Cart');
const Items = require('../../src/Items');

jest.mock('../../src/Cart');
jest.mock('../../src/Items');

beforeEach(() => {
  Cart.mockClear();
  Items.mockClear();
});

/**
 * Sample data for loadCarts method
 */
const loadCartsData = [
  {
    id: 1,
    items: [
      {
        article_id: 1,
        quantity: 6,
      },
      {
        article_id: 2,
        quantity: 2,
      },
      {
        article_id: 4,
        quantity: 1,
      },
    ],
  },
];

/**
 * missing property - data for loadCarts method
 */
const missingPropertyData = [
  {
    id: 1,
  },
];

/**
 * Test suite for Carts constructor
 */
describe('Carts class', () => {
  const carts = new Carts();

  test('is instantiated', () => expect(carts).toBeInstanceOf(Carts));
  test('has list', () => expect(Array.isArray(carts.list)).toBeTruthy());
});

/**
 * Test suites for add() method
 */
describe('add an cart', () => {
  const carts = new Carts();
  const length = carts.list.length;

  test('bad input', () => expect(() => carts.add()).toThrow('bad input'));
  test('bad input', () => expect(() => carts.add(new Object())).toThrow('bad input'));

  const items = new Items();
  const cart = new Cart(1, items);
  carts.add(cart);

  test('effectively added', () => expect(carts.list.length === length + 1).toBeTruthy());
});

/**
 * Test suite for loadCarts method
 */
describe('load carts data', () => {
  try {
    const carts = new Carts();

    test('bad input', () => expect(() => carts.loadCarts()).toThrow('bad input'));
    test('bad input', () => expect(() => carts.loadCarts(new Object())).toThrow('bad input'));

    test('bad format', () =>
      expect(() => carts.loadCarts(missingPropertyData)).toThrow('missing required property in carts data file'));

    const length = carts.list.length;
    const iLength = loadCartsData.length;

    carts.loadCarts([]);
    test('handle empty array', () => expect(carts.list.length === length + iLength).toBeTruthy());

    carts.loadCarts(loadCartsData);
    test('effectively added', () => expect(carts.list.length === length + iLength).toBeTruthy());
  } catch (e) {
    throw `Add of the carts data failed: Error ${e}`;
  }
});
