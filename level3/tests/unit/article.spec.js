/**
 * Test suites for Article class
 */

const Article = require('../../src/Article');

/**
 * Test suite for Article constructor
 */

describe('Article class', () => {
  describe('constructor', () => {
    test('bad inputs', () => {
      expect(() => new Article()).toThrow('bad input');
      expect(() => new Article(1)).toThrow('bad input');
      expect(() => new Article('1')).toThrow('bad input');
      expect(() => new Article(1, 'water')).toThrow('bad input');
      expect(() => new Article(1, 100)).toThrow('bad input');
      expect(() => new Article(1, 'water', '100')).toThrow('bad input');
    });
    test('good inputs', () => expect(new Article(1, 'water', 100)).toBeInstanceOf(Article));
  });

  describe('getPrice()', () => {
    const article = new Article(1, 'water', 100);
    test('good return value', () => expect(article.getPrice()).toBe(100));
  });
});
