/**
 * Test suites for EligibleTransactionVolume class
 */

const EligibleTransactionVolume = require('../../src/EligibleTransactionVolume');

/**
 * Test suite for Article constructor
 */

describe('Eligible Transaction Volume class', () => {
  test('bad inputs', () => {
    expect(() => new EligibleTransactionVolume()).toThrow('bad input');
    expect(() => new EligibleTransactionVolume(new Object())).toThrow('bad input');
    expect(() => new EligibleTransactionVolume(new Object(), new Object())).toThrow('bad input');
  });
  test('good inputs', () => {
    expect(new EligibleTransactionVolume(2000)).toBeInstanceOf(EligibleTransactionVolume);
    expect(new EligibleTransactionVolume(0, 1000)).toBeInstanceOf(EligibleTransactionVolume);
  });
});
