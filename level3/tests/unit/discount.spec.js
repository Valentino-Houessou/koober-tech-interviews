/**
 * Tests suite for discount
 */

const Discount = require('../../src/Discount');

describe('Discount class', () => {
  describe('constructor', () => {
    test('bad inputs', () => {
      expect(() => new Discount()).toThrow('bad input');
      expect(() => new Discount(new Object())).toThrow('bad input');
      expect(() => new Discount(new Object(), new Object())).toThrow('bad input');
      expect(() => new Discount(new Object(), new Object(), new Object())).toThrow('bad input');
    });

    test('good inputs', () => {
      expect(new Discount(2, 'amount', 25)).toBeInstanceOf(Discount);
    });
  });
});
