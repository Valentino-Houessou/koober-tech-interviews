/**
 * Test suites for InputLoader class
 */

const InputLoader = require('../../src/InputLoader');

/**
 * Test bad data path
 * */
const badDataPath = './tests/input.json';

/**
 * Test suite for InputLoader constructor
 */
describe('Input Loader class', () => {
  test("bad constructor's inputs", () => {
    expect(() => new InputLoader()).toThrow('bad input');
    expect(() => new InputLoader(new Object())).toThrow('bad input');
    expect(() => new InputLoader(badDataPath)).toThrow('bad input');
    expect(() => new InputLoader(badDataPath, new Object())).toThrow('bad input');
    expect(() => new InputLoader(badDataPath, new Object(), new Object())).toThrow('bad input');
    expect(() => new InputLoader(badDataPath, new Object(), new Object(), new Object())).toThrow('bad input');
    expect(() => new InputLoader(badDataPath, new Object(), new Object(), new Object(), new Object())).toThrow(
      'bad input'
    );
  });
});
