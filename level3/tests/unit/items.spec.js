/**
 * Test suites for Items class
 */

const Items = require('../../src/Items');
const Item = require('../../src/Item');

jest.mock('../../src/Item');

beforeEach(() => {
  Item.mockClear();
});

/**
 * Sample data for loadItems method
 */
const loadItemsData = [
  {
    article_id: 1,
    quantity: 6,
  },
  {
    article_id: 2,
    quantity: 2,
  },
];

/**
 * missing property - data for loadItems method
 */
const missingPropertyData = [
  {
    article_id: 1,
  },
];

/**
 * Test suite for Items constructor
 */
describe('Items class', () => {
  const items = new Items();

  test('is instantiated', () => expect(items).toBeInstanceOf(Items));
  test('has list', () => expect(Array.isArray(items.list)).toBeTruthy());
});

/**
 * Test suites for add() method
 */
describe('add an item', () => {
  const items = new Items();
  const item = new Item(1, 2);
  const length = items.list.length;

  test('bad input', () => expect(() => items.add()).toThrow('bad input'));
  test('bad input', () => expect(() => items.add(new Object())).toThrow('bad input'));

  items.add(item);

  test('effectively added', () => expect(items.list.length === length + 1).toBeTruthy());
});

/**
 * Test suite for loadItems method
 */
describe('load items data', () => {
  const items = new Items();

  test('bad input', () => expect(() => items.loadItems()).toThrow('bad input'));
  test('bad input', () => expect(() => items.loadItems(new Object())).toThrow('bad input'));

  test('bad format', () =>
    expect(() => items.loadItems(missingPropertyData)).toThrow('missing required property in items data file'));

  const length = items.list.length;
  const iLength = loadItemsData.length;

  items.loadItems([]);
  test('handle empty array', () => expect(items.list.length === length + iLength).toBeTruthy());

  items.loadItems(loadItemsData);
  test('effectively added', () => expect(items.list.length === length + iLength).toBeTruthy());
});

/**
 * Test suite for itemsTotalCost method
 * - Bad input
 */
describe('items total cost - bad input - empty list', () => {
  const items = new Items();

  test('No input given', () => expect(() => items.itemsTotalCost()).toThrow('bad input'));
  test('bad input type', () => expect(() => items.itemsTotalCost(new Object())).toThrow('bad input'));
  test('bad input type', () => expect(() => items.itemsTotalCost(new Object(), new Object())).toThrow('bad input'));
});
