/**
 * Test suite for delivery fees class
 */

const DeliveryFees = require('../../src/DeliveryFees');
const EligibleTransactionVolume = require('../../src/EligibleTransactionVolume');
const DeliveryFee = require('../../src/DeliveryFee');

jest.mock('../../src/EligibleTransactionVolume');
jest.mock('../../src/DeliveryFee');

beforeEach(() => {
  EligibleTransactionVolume.mockClear();
  DeliveryFee.mockClear();
});

/**
 * Load deliveryFees data
 */
const loadDeliveryFeesData = [
  {
    eligible_transaction_volume: {
      min_price: 0,
      max_price: 1000,
    },
    price: 800,
  },
  {
    eligible_transaction_volume: {
      min_price: 2000,
      max_price: null,
    },
    price: 0,
  },
];

/**
 * Missing price property in data
 */
const missingPriceData = [
  {
    eligible_transaction_volume: {
      min_price: 0,
      max_price: 1000,
    },
  },
];

/**
 * missing Eligible Transaction Volume property in Data
 */
const missingEligibleTransactionVolumeData = [
  {
    price: 800,
  },
];

/**
 * missing minprice property in EligibleTransactionVolume Data
 */
const missingMinPriceData = [
  {
    eligible_transaction_volume: {
      max_price: 1000,
    },
    price: 800,
  },
];

/**
 * Test suite for Items constructor
 */
describe('DeliveryFees class', () => {
  const deliveryFees = new DeliveryFees();

  test('is instantiated', () => expect(deliveryFees).toBeInstanceOf(DeliveryFees));
  test('has list', () => expect(Array.isArray(deliveryFees.list)).toBeTruthy());
});

/**
 * Test suites for add() method
 */
describe('add an DeliveryFee', () => {
  const deliveryFees = new DeliveryFees();
  const eTV = new EligibleTransactionVolume(0, 1000);
  const deliveryFee = new DeliveryFee(eTV, 800);

  const length = deliveryFees.list.length;

  test('bad input', () => expect(() => deliveryFees.add()).toThrow('bad input'));
  test('bad input', () => expect(() => deliveryFees.add(new Object())).toThrow('bad input'));

  deliveryFees.add(deliveryFee);

  test('effectively added', () => expect(deliveryFees.list.length === length + 1).toBeTruthy());
});

/**
 * Test suite for loadDeliveryFees method
 */
describe('load deliveryFees data', () => {
  const deliveryFees = new DeliveryFees();

  test('bad input', () => expect(() => deliveryFees.loadDeliveryFees()).toThrow('bad input'));
  test('bad input', () => expect(() => deliveryFees.loadDeliveryFees(new Object())).toThrow('bad input'));

  test('bad format', () =>
    expect(() => deliveryFees.loadDeliveryFees(missingPriceData)).toThrow(
      'missing required property in deliveryFees data file'
    ));

  test('bad format', () =>
    expect(() => deliveryFees.loadDeliveryFees(missingEligibleTransactionVolumeData)).toThrow(
      'missing required property in deliveryFees data file'
    ));

  test('bad format', () =>
    expect(() => deliveryFees.loadDeliveryFees(missingMinPriceData)).toThrow(
      'missing required property in deliveryFees data file'
    ));

  deliveryFees.loadDeliveryFees([]);
  test('handle empty array', () => expect(deliveryFees.list.length).toBe(0));
});

describe('load deliveryFees data - effective', () => {
  test('effectively added', () => {
    const deliveryFees = new DeliveryFees();

    const length = deliveryFees.list.length;
    const iLenght = loadDeliveryFeesData.length;

    deliveryFees.loadDeliveryFees(loadDeliveryFeesData);

    expect(deliveryFees.list.length === length + iLenght).toBeTruthy();
    expect(DeliveryFee).toHaveBeenCalledTimes(2);
  });
});

/**
 * Tests suite for getDeliveryPrice method
 */
describe('getDeliveryPrice()', () => {
  describe('get an cart delivery price - bad input', () => {
    const deliveryFees = new DeliveryFees();
    test('bad input', () => expect(() => deliveryFees.getDeliveryPrice()).toThrow('bad input'));
    test('bad input type', () => expect(() => deliveryFees.getDeliveryPrice(new Object())).toThrow('bad input'));
  });
});
