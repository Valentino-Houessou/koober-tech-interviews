/**
 * Eligible transaction volume
 */
module.exports = class EligibleTransactionVolume {
  /**
   * Represent EligibleTransactionVolume
   * @constructor
   * @param {number} minPrice : minimum price
   * @param {number} maxPrice : maximum price
   */
  constructor(minPrice, maxPrice) {
    if (typeof minPrice === 'number') {
      this.minPrice = minPrice;
    } else {
      throw new Error('bad input');
    }
    if (typeof maxPrice === 'number') {
      this.maxPrice = maxPrice;
    } else {
      this.maxPrice = Infinity;
    }
  }
};
