/**
 * Items class. Manage a list of items
 */

const Item = require('./Item');
const Articles = require('./Articles');
const Discounts = require('./Discounts');

module.exports = class Items {
  /**
   * Represent a list of Items
   * @constructor
   */
  constructor() {
    this.list = [];
  }

  /**
   * Add an item to items list
   * @param {Item} item: object of type Item
   */
  add(item) {
    if (item instanceof Item) this.list.push(item);
    else throw new Error('bad input');
  }

  /**
   * Load an array of object of items data
   * @param {Array} data: array containing items data
   */
  loadItems(data) {
    if (Array.isArray(data)) {
      data.forEach((elt) => {
        if (typeof elt.article_id !== 'undefined' && elt.quantity) {
          this.add(new Item(elt.article_id, elt.quantity));
        } else {
          throw new Error('missing required property in items data file');
        }
      });
    } else {
      throw new Error('bad input');
    }
  }

  /**
   * Process the items total cost
   * @param {Articles} articles: Articles instance
   * @param {Discounts} discounts: Discounts instance
   * @return {number} : cost of the cart
   */
  itemsTotalCost(articles, discounts) {
    if (articles instanceof Articles && discounts instanceof Discounts) {
      if (this.list.length > 0) {
        return this.list.reduce((accumulator, item) => {
          const articleId = item.articleId;
          const article = articles.getArticle(articleId);
          let price = article ? article.getPrice() : 0;
          const discount = discounts.getDiscount(articleId);

          if (discount) {
            if (discount.isPercentage) price = Math.floor(price - (price * discount.value) / 100);
            if (discount.isAmount) price -= discount.value;
          }

          return (accumulator += price * item.quantity);
        }, 0);
      } else {
        return 0;
      }
    } else {
      throw new Error('bad input');
    }
  }
};
