/**
 * Class representing list of delivery Fee
 */

const DeliveryFee = require('./DeliveryFee');
const EligibleTransactionVolume = require('./EligibleTransactionVolume');

module.exports = class DeliveryFees {
  /**
   * Represent list of delivery fee
   * @constructor
   */
  constructor() {
    this.list = [];
  }

  /**
   * Add an DeliveryFee to DeliveryFees list
   * @param {DeliveryFee} DeliveryFee: object of type DeliveryFee
   */
  add(deliveryFee) {
    if (deliveryFee instanceof DeliveryFee) this.list.push(deliveryFee);
    else throw new Error('bad input');
  }

  /**
   * Load an array of object of deliveryFee data
   * @param {Array} data: array containing deliveryFees data
   */
  loadDeliveryFees(data) {
    if (Array.isArray(data)) {
      data.forEach((elt) => {
        // eligible_transaction_volume within data
        const eTV = elt.eligible_transaction_volume;
        const price = elt.price;

        if (eTV && typeof eTV.min_price !== 'undefined' && typeof price !== 'undefined') {
          const minPrice = eTV.min_price;
          const maxPrice = eTV.max_price;
          const eligibleTransactionVolume = new EligibleTransactionVolume(minPrice, maxPrice);
          const deliveryFee = new DeliveryFee(eligibleTransactionVolume, price);

          this.add(deliveryFee);
        } else {
          throw new Error('missing required property in deliveryFees data file');
        }
      });
    } else {
      throw new Error('bad input');
    }
  }

  /**
   * Get delivery price based on the cart price
   * @param {number} cartPrice: price of the cart
   * @return {number}
   */
  getDeliveryPrice(cartPrice) {
    if (typeof cartPrice === 'number') {
      if (this.list.length > 0) {
        return this.list.find(
          (elt) =>
            cartPrice >= elt.eligibleTransactionVolume.minPrice && cartPrice < elt.eligibleTransactionVolume.maxPrice
        ).price;
      } else {
        return 0;
      }
    } else {
      throw new Error('bad input');
    }
  }
};
