/**
 * Articles class. manage a list of article
 */
const Article = require('./Article');

module.exports = class Articles {
  /**
   * Represent a list of Article
   * @constructor
   */
  constructor() {
    this.list = [];
  }

  /**
   * Add an article to articles list
   * @param {Article} article : object of type article
   */
  add(article) {
    if (article instanceof Article) this.list.push(article);
    else throw new Error('bad input');
  }

  /**
   * Load an array of object of articles data
   * @param {Array} data : array containing articles data
   */
  loadArticles(data) {
    if (Array.isArray(data)) {
      data.forEach((elt) => {
        if (typeof elt.id !== 'undefined' && elt.name && elt.price !== 'undefined') {
          this.add(new Article(elt.id, elt.name, elt.price));
        } else {
          throw new Error('missing required property in articles data file');
        }
      });
    } else {
      throw new Error('bad input');
    }
  }

  /**
   * Get an article based on given Id
   * @param {number} articleId
   * @returns {Article}
   */
  getArticle(articleId) {
    if (typeof articleId === 'number') {
      return this.list.find((elt) => elt.id === articleId);
    } else {
      throw new Error('bad input');
    }
  }
};
