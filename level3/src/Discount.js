/**
 * Class representing discount
 */

module.exports = class Discount {
  /**
   * Represent a discount
   * @constructor
   * @param {number} articleId
   * @param {string} type
   * @param {number} value
   */
  constructor(articleId, type, value) {
    if (typeof articleId === 'number' && typeof type === 'string' && typeof value === 'number')
      (this.articleId = articleId),
        (this.type = type),
        (this.value = value),
        (this.isPercentage = type === 'percentage'),
        (this.isAmount = type === 'amount');
    else throw new Error('bad input');
  }
};
