/**
 * Discounts class. manage a list of discount
 */

const Discount = require('./Discount');

module.exports = class Discounts {
  /**
   * Represent a list of Discount
   * @constructor
   */
  constructor() {
    this.list = [];
  }

  /**
   * Add an discount to discounts list
   * @param {Discount} discount : object of type article
   */
  add(discount) {
    if (discount instanceof Discount) this.list.push(discount);
    else throw new Error('bad input');
  }

  /**
   * Load an array of object of discounts data
   * @param {Array} data : array containing discounts data
   */
  loadDiscounts(data) {
    if (Array.isArray(data)) {
      data.forEach((elt) => {
        if (typeof elt.article_id !== 'undefined' && elt.type && elt.value !== 'undefined') {
          this.add(new Discount(elt.article_id, elt.type, elt.value));
        } else {
          throw new Error('missing required property in discounts data file');
        }
      });
    } else {
      throw new Error('bad input');
    }
  }

  /**
   * Get an discount based on given articleId
   * @param {number} articleId
   * @returns {Discount}
   */
  getDiscount(articleId) {
    if (typeof articleId === 'number') {
      return this.list.find((elt) => elt.articleId === articleId);
    } else {
      throw new Error('bad input');
    }
  }
};
