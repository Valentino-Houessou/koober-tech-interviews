/**
 * InputLoader class. Load the given input file
 */
const fs = require('fs');

const Articles = require('./Articles');
const Carts = require('./Carts');
const DeliveryFees = require('./DeliveryFees');
const Discounts = require('./Discounts');

module.exports = class InputLoader {
  /**
   * Represent an input loader
   * @constructor
   * @param {string} path
   * @param {Articles} articles
   * @param {Carts} carts
   * @param {DeliveryFees} deliveryFees
   * @param {Discounts} discounts
   */
  constructor(path, articles, carts, deliveryFees, discounts) {
    if (
      typeof path === 'string' &&
      articles instanceof Articles &&
      carts instanceof Carts &&
      deliveryFees instanceof DeliveryFees &&
      discounts instanceof Discounts
    ) {
      return new Promise(async (resolve, reject) => {
        fs.readFile(path, (err, data) => {
          if (err) return reject(new Error("Bad file's path"));

          const loadedData = JSON.parse(data);
          const jArticles = loadedData.articles;
          const jCarts = loadedData.carts;
          const jDeliveryFees = loadedData.delivery_fees;
          const jDiscounts = loadedData.discounts;

          if (jArticles && jCarts && jDeliveryFees && jDiscounts) {
            try {
              articles.loadArticles(jArticles);
              carts.loadCarts(jCarts);
              deliveryFees.loadDeliveryFees(jDeliveryFees);
              discounts.loadDiscounts(jDiscounts);

              this.articles = articles;
              this.carts = carts;
              this.deliveryFees = deliveryFees;
              this.discounts = discounts;

              resolve(this);
            } catch (e) {
              reject(e);
            }
          } else {
            return reject(new Error('missing required property in input data file'));
          }
        });
      });
    } else throw new Error('bad input');
  }
};
