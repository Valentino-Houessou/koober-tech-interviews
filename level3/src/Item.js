/**
 * Item class.
 */

module.exports = class Item {
  /**
   * Represent an Item
   * @constructor
   * @param {number} articleId
   * @param {number} quantity
   */
  constructor(articleId, quantity) {
    if (typeof articleId === 'number' && typeof quantity === 'number')
      (this.articleId = articleId), (this.quantity = quantity);
    else throw new Error('bad input');
  }
};
