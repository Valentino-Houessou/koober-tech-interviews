/**
 * Test suites for InputLoader class
 */

const InputLoader = require('../../src/InputLoader');
const Articles = require('../../src/Articles');
const Carts = require('../../src/Carts');

/**
 * Test data path
 */
const dataPath = './tests/fakeData/input.json';

/**
 * Test bad data path
 * */
const badDataPath = './tests/input.json';

/**
 * Test missing data path
 * */
const missingDataPath = './tests/fakeData/missingInput.json';

/**
 * Test missing property data path
 * */
const missingPropertyDataPath = './tests/fakeData/missingPropertyInput.json';

/**
 * Test suite for InputLoader constructor
 */
describe('Input Loader class', () => {
  const articles = new Articles();
  const carts = new Carts();

  test("bad constructor's inputs", async () =>
    await expect(new InputLoader(badDataPath, articles, carts)).rejects.toThrow("Bad file's path"));
  test('bad format', async () =>
    await expect(new InputLoader(missingDataPath, articles, carts)).rejects.toThrow(
      'missing required property in input data file'
    ));
  test('One variable property is missing', async () =>
    await expect(new InputLoader(missingPropertyDataPath, articles, carts)).rejects.toThrow());
  test("good constructor's inputs", async () =>
    await expect(new InputLoader(dataPath, articles, carts)).resolves.toBeInstanceOf(InputLoader));
});
