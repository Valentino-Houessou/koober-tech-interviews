/**
 * Test suites for Item class
 */

const Item = require('../../src/Item');

/**
 * Test suite for Item constructor
 */
describe('Item class', () => {
  test('bad inputs', () => {
    expect(() => new Item()).toThrow('bad input');
    expect(() => new Item(1)).toThrow('bad input');
    expect(() => new Item('1')).toThrow('bad input');
    expect(() => new Item(1, '1')).toThrow('bad input');
  });
  test('good inputs', () => expect(new Item(1, 2)).toBeInstanceOf(Item));
});
