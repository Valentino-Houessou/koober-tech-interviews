/**
 * Test suites for Cart class
 */

const Cart = require('../../src/Cart');
const Items = require('../../src/Items');

jest.mock('../../src/Items');

beforeEach(() => {
  Items.mockClear();
});

/**
 * Test suite for Cart Constructor
 */

describe('Cart class', () => {
  test('bad inputs', () => {
    expect(() => new Cart()).toThrow('bad input');
    expect(() => new Cart(1)).toThrow('bad input');
    expect(() => new Cart('1')).toThrow('bad input');
    expect(() => new Cart(1, new Object())).toThrow('bad input');
  });
  const items = new Items();
  test('good inputs', () => expect(new Cart(1, items)).toBeInstanceOf(Cart));
});
