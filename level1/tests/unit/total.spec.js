/**
 * Test suites for Total class
 */

const Total = require('../../src/Total');
const CartTotal = require('../../src/CartTotal');

jest.mock('../../src/CartTotal');

beforeEach(() => {
  CartTotal.mockClear();
});

/**
 * Test suite for Total constructor
 */
describe('Total class', () => {
  const total = new Total();

  test('is instantiated', () => expect(total).toBeInstanceOf(Total));
  test('has list', () => expect(Array.isArray(total.carts)).toBeTruthy());
});

/**
 * Test suites for add() method
 */
describe('add an cart', () => {
  const total = new Total();
  const length = total.carts.length;

  test('No input given', () => expect(() => total.add()).toThrow('bad input'));
  test('bad input type', () => expect(() => total.add(new Object())).toThrow('bad input'));

  const cartTotal = new CartTotal(1, 1000);

  total.add(cartTotal);

  test('effectively added', () => expect(total.carts.length === length + 1).toBeTruthy());
});

/**
 * Test suites for cartsTotal() method
 * - Bad input
 */
describe('process carts total - Bad input - empty carts and articles', () => {
  const total = new Total();
  test('No input given', () => expect(() => total.cartsTotal()).toThrow('bad input'));
  test('bad input type', () => expect(() => total.cartsTotal(new Object())).toThrow('bad input'));
  test('bad input type', () => expect(() => total.cartsTotal(new Object(), new Object())).toThrow('bad input'));
});
