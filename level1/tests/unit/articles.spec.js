/**
 * Test suites for Articles class
 */

const Articles = require('../../src/Articles');
const Article = require('../../src/Article');

jest.mock('../../src/Article');

beforeEach(() => {
  Article.mockClear();
});

/**
 * Sample data for loadArticles method
 */
const loadArcticlesData = [
  {
    id: 1,
    name: 'water',
    price: 100,
  },
  {
    id: 2,
    name: 'honey',
    price: 200,
  },
];

/**
 * missing property - data for loadArticles method
 */
const missingPropertyData = [
  {
    id: 1,
    price: 100,
  },
];

/**
 * Test suite for articles constructor
 */
describe('articles class', () => {
  const articles = new Articles();

  test('is instantiated', () => expect(articles).toBeInstanceOf(Articles));
  test('has list', () => expect(Array.isArray(articles.list)).toBeTruthy());
});

/**
 * Test suite for add() method
 */
describe('add an article', () => {
  const articles = new Articles();
  const article = new Article(1, 'water', 100);
  const length = articles.list.length;

  test('No input given', () => expect(() => articles.add()).toThrow('bad input'));
  test('bad input type', () => expect(() => articles.add(new Object())).toThrow('bad input'));

  articles.add(article);

  test('effectively added', () => expect(articles.list.length === length + 1).toBeTruthy());
});

/**
 * Test suite for loadArticles method
 */
describe('load articles data', () => {
  const articles = new Articles();

  test('No input given', () => expect(() => articles.loadArticles()).toThrow('bad input'));
  test('bad input type', () => expect(() => articles.loadArticles(new Object())).toThrow('bad input'));

  test('bad format', () =>
    expect(() => articles.loadArticles(missingPropertyData)).toThrow(
      'missing required property in articles data file'
    ));

  articles.loadArticles([]);
  test('handle empty array', () => expect(articles.list.length).toBe(0));
});

describe('load articles data - effective', () => {
  test('effectively added', () => {
    const articles = new Articles();

    const length = articles.list.length;
    const iLenght = loadArcticlesData.length;

    articles.loadArticles(loadArcticlesData);

    expect(articles.list.length === length + iLenght).toBeTruthy();
    expect(Article).toHaveBeenCalledTimes(2);
  });
});

/**
 * Tests suite for getArticle
 */
describe('getArticle()', () => {
  describe('get an article - bad input', () => {
    const articles = new Articles();
    test('No input', () => expect(() => articles.getArticle()).toThrow('bad input'));
    test('bad input type', () => expect(() => articles.getArticle(new Object())).toThrow('bad input'));

    test('not found', () => expect(articles.getArticle(1)).toBeUndefined());
  });
});
