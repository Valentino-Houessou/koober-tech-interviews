/**
 * Cart class
 */

const Items = require('./Items');

module.exports = class Cart {
  /**
   * Represent a cart
   * @constructor
   * @param {integer} id
   * @param {Items} items
   */
  constructor(id, items) {
    if (typeof id === 'number' && items instanceof Items) (this.id = id), (this.items = items);
    else throw new Error('bad input');
  }
};
