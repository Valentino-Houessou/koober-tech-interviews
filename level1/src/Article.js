/** Article class */
module.exports = class Article {
  /**
   * Represent an article
   * @constructor
   * @param {number} id : id of the article
   * @param {*} name : name of the aricle
   * @param {*} price : price of the article
   */
  constructor(id, name, price) {
    if (typeof id === 'number' && typeof name === 'string' && typeof price === 'number')
      (this.id = id), (this.name = name), (this.price = price);
    else throw new Error('bad input');
  }

  /**
   * Get the price of an article
   */
  getPrice() {
    return this.price;
  }
};
