/**
 * InputLoader class. Load the given input file
 */
const fs = require('fs');

const Articles = require('./Articles');
const Carts = require('./Carts');

module.exports = class InputLoader {
  /**
   * Represent an input loader
   * @constructor
   * @param {string} path
   * @param {Articles} articles
   * @param {Carts} carts
   */
  constructor(path, articles, carts) {
    if (typeof path === 'string' && articles instanceof Articles && carts instanceof Carts) {
      return new Promise(async (resolve, reject) => {
        fs.readFile(path, (err, data) => {
          if (err) return reject(new Error("Bad file's path"));

          const loadedData = JSON.parse(data);

          if (loadedData.articles && loadedData.carts) {
            try {
              articles.loadArticles(loadedData.articles);
              carts.loadCarts(loadedData.carts);

              this.articles = articles;
              this.carts = carts;

              resolve(this);
            } catch (e) {
              reject(new Error(e.message));
            }
          } else {
            return reject(new Error('missing required property in input data file'));
          }
        });
      });
    } else throw new Error('bad input');
  }
};
