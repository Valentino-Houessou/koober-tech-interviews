/**
 * Total class contains the cartTotal for each cart
 */
const CartTotal = require('./CartTotal');
const Articles = require('./Articles');
const Carts = require('./Carts');

module.exports = class Total {
  /**
   * Represent the total
   */
  constructor() {
    this.carts = [];
  }

  /**
   * Add an cartTotal to carts
   * @param {CartTotal} cartTotal
   */
  add(cartTotal) {
    if (cartTotal instanceof CartTotal) {
      this.carts.push(cartTotal);
    } else {
      throw 'bad input';
    }
  }

  /**
   * Update a total object for a given carts based on a given articles
   * @param {Articles} articles
   * @param {Carts} carts
   */
  cartsTotal(articles, carts) {
    if (articles instanceof Articles && carts instanceof Carts) {
      if (carts.list.length > 0 && articles.list.length > 0) {
        carts.list.forEach((elt) => {
          const total = elt.items.itemsTotalCost(articles);
          const cartTotal = new CartTotal(elt.id, total);
          this.carts.push(cartTotal);
        });
      }
    } else {
      throw 'bad input';
    }
  }
};
