/**
 * CartTotal contain a cart id and its total
 */
module.exports = class CartTotal {
  /**
   * Represent a cart total
   * @constructor
   * @param {number} id
   * @param {number} total
   */
  constructor(id, total) {
    if (typeof id === 'number' && typeof total === 'number') {
      (this.id = id), (this.total = total);
    } else {
      throw new Error('bad input');
    }
  }
};
