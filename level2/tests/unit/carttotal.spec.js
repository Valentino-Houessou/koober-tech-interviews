/**
 * Test suites for CartTotal class
 */

const CartTotal = require('../../src/CartTotal');

/**
 * Test suite for Cart Constructor
 */

describe('CartTotal class', () => {
  test('bad inputs', () => {
    expect(() => new CartTotal()).toThrow('bad input');
    expect(() => new CartTotal(1)).toThrow('bad input');
    expect(() => new CartTotal('1')).toThrow('bad input');
    expect(() => new CartTotal(1, new Object())).toThrow('bad input');
  });
  test('good inputs', () => expect(new CartTotal(1, 100)).toBeInstanceOf(CartTotal));
});
