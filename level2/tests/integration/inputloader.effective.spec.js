/**
 * Test suites for InputLoader class
 */

const InputLoader = require('../../src/InputLoader');
const Articles = require('../../src/Articles');
const Carts = require('../../src/Carts');
const DeliveryFees = require('../../src/DeliveryFees');

/**
 * Test data path
 */
const dataPath = './tests/fakeData/input.json';

/**
 * Test bad data path
 * */
const badDataPath = './tests/input.json';

/**
 * Test missing data path
 * */
const missingDataPath = './tests/fakeData/missingInput.json';

/**
 * Test missing property data path
 * */
const missingPropertyDataPath = './tests/fakeData/missingPropertyInput.json';

/**
 * Test suite for InputLoader constructor
 */
describe('Input Loader class', () => {
  const articles = new Articles();
  const carts = new Carts();
  const deliveryFees = new DeliveryFees();

  test("bad constructor's inputs", async () =>
    await expect(new InputLoader(badDataPath, articles, carts, deliveryFees)).rejects.toThrow("Bad file's path"));
  test('bad format', async () =>
    await expect(new InputLoader(missingDataPath, articles, carts, deliveryFees)).rejects.toThrow(
      'missing required property in input data file'
    ));
  test('One variable property is missing', async () =>
    await expect(new InputLoader(missingPropertyDataPath, articles, carts, deliveryFees)).rejects.toThrow());
  test("good constructor's inputs", async () =>
    await expect(new InputLoader(dataPath, articles, carts, deliveryFees)).resolves.toBeInstanceOf(InputLoader));
});
