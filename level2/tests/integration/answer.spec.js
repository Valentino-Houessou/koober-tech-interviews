/**
 * Global integration test suite to test the feature of level 1
 */

const answer = require('../../src/Answer');

/**
 * Test data path
 */
const dataPath = './tests/fakeData/input.json';

/**
 * Bad data path
 * */
const badDataPath = './tests/input.json';

/**
 * Missing data path
 * */
const missingDataPath = './tests/fakeData/missingInput.json';

/**
 * Missing property data path
 * */
const missingPropertyDataPath = './tests/fakeData/missingPropertyInput.json';

/**
 * Empty articles variable - data path
 * */
const emptyArticlesDataPath = './tests/fakeData/emptyArticlesInput.json';

/**
 * Empty carts variable - data path
 * */
const emptyCartsDataPath = './tests/fakeData/emptyCartsInput.json';

/**
 * Empty deliveryFees variable - data path
 * */
const emptyDeliveryFeeDataPath = './tests/fakeData/emptyDeliveryFeesInput.json';

/**
 * Empty articles and carts - data path
 * */
const emptyInputDataPath = './tests/fakeData/emptyInput.json';

/**
 * Empty result
 */
const emptyResult = {
  carts: [],
};

/**
 * Test Result
 */
const withoutDeliveryFeeResult = {
  carts: [
    {
      id: 1,
      total: 1000,
    },
  ],
};

/**
 * Test Result
 */
const result = {
  carts: [
    {
      id: 1,
      total: 1400,
    },
  ],
};

/**
 * Input file path should be a string
 */
test('path should be a correct string', async () => {
  await expect(() => answer()).rejects.toThrow('bad input');
  await expect(() => answer(1)).rejects.toThrow('bad input');
  await expect(() => answer(new Object())).rejects.toThrow('bad input');
  await expect(() => answer(badDataPath)).rejects.toThrow("Bad file's path");
});

/**
 * Input file should contains articles and carts
 */
test('Missing variable in input', async () => await expect(answer(missingDataPath)).rejects.toThrow());

/**
 * The variables in input file should have a specific format
 */
test('Missing property in input', async () => await expect(answer(missingPropertyDataPath)).rejects.toThrow());

/**
 * The input is correct
 */
describe('good answer result', () => {
  test('Empty articles variable', async () =>
    await expect(answer(emptyArticlesDataPath)).resolves.toEqual(emptyResult));

  test('Empty carts variable', async () => await expect(answer(emptyCartsDataPath)).resolves.toEqual(emptyResult));

  test('Empty deliveryFees variable', async () =>
    await expect(answer(emptyDeliveryFeeDataPath)).resolves.toEqual(withoutDeliveryFeeResult));

  test('Empty carts, articles and deliveryFees variables', async () =>
    await expect(answer(emptyInputDataPath)).resolves.toEqual(emptyResult));

  test('Effectively processed', async () => await expect(answer(dataPath)).resolves.toEqual(result));
});
