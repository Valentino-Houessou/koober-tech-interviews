/**
 * Test suites for Total class
 */

const Total = require('../../src/Total');
const Carts = require('../../src/Carts');
const Articles = require('../../src/Articles');
const DeliveryFees = require('../../src/DeliveryFees');

/**
 * Sample data for loadArticles method
 */
const loadArcticlesData = [
  {
    id: 1,
    name: 'water',
    price: 100,
  },
  {
    id: 2,
    name: 'honey',
    price: 200,
  },
];

/**
 * Sample data for loadCarts method
 */
const loadCartsData = [
  {
    id: 1,
    items: [
      {
        article_id: 1,
        quantity: 6,
      },
      {
        article_id: 2,
        quantity: 2,
      },
    ],
  },
];

/**
 * Sample data for loadDeliveryFeesData method
 */
const loadDeliveryFeesData = [
  {
    eligible_transaction_volume: {
      min_price: 0,
      max_price: 1500,
    },
    price: 800,
  },
  {
    eligible_transaction_volume: {
      min_price: 1500,
      max_price: null,
    },
    price: 0,
  },
];

/**
 * return example
 */
const returnString = '[{"id":1,"total":1800}]';

/**
 * Test suites for cartsTotal() method
 * - empty carts and articles
 */
describe('process carts total - Bad input - empty carts and articles', () => {
  const total = new Total();
  const carts = new Carts();
  const articles = new Articles();
  const deliveryFees = new DeliveryFees();

  total.cartsTotal(articles, carts, deliveryFees);

  test('empty carts', () => expect(total.carts.length).toBe(0));
});

/**
 * Test suites for cartsTotal() method
 * - Good input input
 */
describe('process carts total - Good input', () => {
  const total = new Total();
  const carts = new Carts();
  const articles = new Articles();
  const deliveryFees = new DeliveryFees();

  articles.loadArticles(loadArcticlesData);
  carts.loadCarts(loadCartsData);
  deliveryFees.loadDeliveryFees(loadDeliveryFeesData);

  total.cartsTotal(articles, carts, deliveryFees);

  test('not empty carts', () => expect(total.carts.length).toBe(1));
  test('effectively added', () => expect(JSON.stringify(total.carts)).toBe(returnString));
});
