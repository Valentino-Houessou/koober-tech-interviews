/**
 * Test suites for Items class
 */

const Items = require('../../src/Items');
const Articles = require('../../src/Articles');

/**
 * Sample data for loadItems method
 */
const loadItemsData = [
  {
    article_id: 1,
    quantity: 6,
  },
  {
    article_id: 2,
    quantity: 2,
  },
];

/**
 * Sample data for loadBadItems method
 */
const loadBadItemsData = [
  {
    article_id: 3,
    quantity: 6,
  },
  {
    article_id: 2,
    quantity: 2,
  },
];

/**
 * Sample data for loadArticles method
 */
const loadArcticlesData = [
  {
    id: 1,
    name: 'water',
    price: 100,
  },
  {
    id: 2,
    name: 'honey',
    price: 200,
  },
];

/**
 * Test suite for itemsTotalCost method
 * - empty items list
 */
describe('items total cost - bad input - empty list', () => {
  try {
    const items = new Items();

    const articles = new Articles();

    test('empty items list', () => expect(items.itemsTotalCost(articles)).toBe(0));
  } catch (error) {
    throw `items total cost process failed: Error ${error}`;
  }
});

/**
 * Test suite for itemsTotalCost method
 * - Missing items id in articles
 */
describe('items total cost - effective', () => {
  try {
    const items = new Items();

    const articles = new Articles();
    articles.loadArticles(loadArcticlesData);
    items.loadItems(loadBadItemsData);

    test('Only good items cost', () => expect(items.itemsTotalCost(articles)).toBe(400));
  } catch (error) {
    throw `items total cost process failed: Error ${error}`;
  }
});

/**
 * Test suite for itemsTotalCost method
 * - Effective
 */
describe('items total cost - effective', () => {
  try {
    const items = new Items();

    const articles = new Articles();
    articles.loadArticles(loadArcticlesData);
    items.loadItems(loadItemsData);

    test('effective cost', () => expect(items.itemsTotalCost(articles)).toBe(1000));
  } catch (error) {
    throw `items total cost process failed: Error ${error}`;
  }
});
