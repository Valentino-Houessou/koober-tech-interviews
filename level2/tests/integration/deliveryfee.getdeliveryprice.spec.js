/**
 * Test suites for get delivery price method
 */

const DeliveryFees = require('../../src/DeliveryFees');
/**
 * Sample data for loadDeliveryFees method
 */
const loadDeliveryFeesData = [
  {
    eligible_transaction_volume: {
      min_price: 0,
      max_price: 1000,
    },
    price: 800,
  },
  {
    eligible_transaction_volume: {
      min_price: 2000,
      max_price: null,
    },
    price: 0,
  },
];

/**
 * Tests suite for getDeliveryPrice method
 */
describe('getDeliveryPrice()', () => {
  describe('get an article price - effective', () => {
    const deliveryFees = new DeliveryFees();

    deliveryFees.loadDeliveryFees(loadDeliveryFeesData);

    test('low cost cart delivery fee, found', () => expect(deliveryFees.getDeliveryPrice(600)).toBe(800));
    test('hight cost cart delivery fee, found', () => expect(deliveryFees.getDeliveryPrice(2200)).toBe(0));
  });
});
