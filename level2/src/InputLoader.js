/**
 * InputLoader class. Load the given input file
 */
const fs = require('fs');

const Articles = require('./Articles');
const Carts = require('./Carts');
const DeliveryFees = require('./DeliveryFees');

module.exports = class InputLoader {
  /**
   * Represent an input loader
   * @constructor
   * @param {string} path
   * @param {Articles} articles
   * @param {Carts} carts
   * @param {DeliveryFees} deliveryFees
   */
  constructor(path, articles, carts, deliveryFees) {
    if (
      typeof path === 'string' &&
      articles instanceof Articles &&
      carts instanceof Carts &&
      deliveryFees instanceof DeliveryFees
    ) {
      return new Promise(async (resolve, reject) => {
        fs.readFile(path, (err, data) => {
          if (err) return reject(new Error("Bad file's path"));

          const loadedData = JSON.parse(data);
          const jArticles = loadedData.articles;
          const jCarts = loadedData.carts;
          const jDeliveryFees = loadedData.delivery_fees;

          if (jArticles && jCarts && jDeliveryFees) {
            try {
              articles.loadArticles(jArticles);
              carts.loadCarts(jCarts);
              deliveryFees.loadDeliveryFees(jDeliveryFees);

              this.articles = articles;
              this.carts = carts;
              this.deliveryFees = deliveryFees;

              resolve(this);
            } catch (e) {
              reject(new Error(e.message));
            }
          } else {
            return reject(new Error('missing required property in input data file'));
          }
        });
      });
    } else throw new Error('bad input');
  }
};
