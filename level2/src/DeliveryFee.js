/**
 * DeliveryFee class
 */

const EligibleTransactionVolume = require('./EligibleTransactionVolume');

module.exports = class DeliveryFee {
  /**
   * Represent Delivery fee
   * @constructor
   * @param {EligibleTransactionVolume} eligibleTransactionVolume: instance of EligibleTransactionVolume
   * @param {number} price: corresponding fee
   */
  constructor(eligibleTransactionVolume, price) {
    if (eligibleTransactionVolume instanceof EligibleTransactionVolume && typeof price === 'number') {
      this.eligibleTransactionVolume = eligibleTransactionVolume;
      this.price = price;
    } else {
      throw new Error('bad input');
    }
  }
};
