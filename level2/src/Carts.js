/**
 * Carts class. Manage a list of carts
 */

const Cart = require('./Cart');
const Items = require('./Items');

module.exports = class Carts {
  /**
   * Represent a list of cart
   * @constructor
   */
  constructor() {
    this.list = [];
  }

  /**
   * Add an cart to carts list
   * @returns: True if cart is effectively added false if not
   * @param {Cart} cart: object of type Cart
   */
  add(cart) {
    if (cart instanceof Cart) this.list.push(cart);
    else throw new Error('bad input');
  }

  /**
   * Load an array of object of carts data
   * @param {Array} data: array containing carts data
   */
  loadCarts(data) {
    if (Array.isArray(data)) {
      data.forEach((elt) => {
        if (typeof elt.id !== 'undefined' && elt.items) {
          const items = new Items();
          items.loadItems(elt.items);
          const cart = new Cart(elt.id, items);
          this.add(cart);
        } else {
          throw new Error('missing required property in carts data file');
        }
      });
    } else {
      throw new Error('bad input');
    }
  }
};
