/**
 * Total class contains the cartTotal for each cart
 */
const CartTotal = require('./CartTotal');
const Articles = require('./Articles');
const Carts = require('./Carts');
const DeliveryFees = require('./DeliveryFees');

module.exports = class Total {
  /**
   * Represent the total
   */
  constructor() {
    this.carts = [];
  }

  /**
   * Add an cartTotal to carts
   * @param {CartTotal} cartTotal
   */
  add(cartTotal) {
    if (cartTotal instanceof CartTotal) {
      this.carts.push(cartTotal);
    } else {
      throw 'bad input';
    }
  }

  /**
   * Update a total object for a given carts based on a given articles and given deliveryFees
   * @param {Articles} articles
   * @param {Carts} carts
   * @param {DeliveryFees} deliveryFees
   */
  cartsTotal(articles, carts, deliveryFees) {
    if (articles instanceof Articles && carts instanceof Carts && deliveryFees instanceof DeliveryFees) {
      if (carts.list.length > 0 && articles.list.length > 0) {
        carts.list.forEach((elt) => {
          const grossTotal = elt.items.itemsTotalCost(articles);
          const deliveryFee = deliveryFees.getDeliveryPrice(grossTotal);
          const total = grossTotal + deliveryFee;
          const cartTotal = new CartTotal(elt.id, total);
          this.carts.push(cartTotal);
        });
      }
    } else {
      throw 'bad input';
    }
  }
};
