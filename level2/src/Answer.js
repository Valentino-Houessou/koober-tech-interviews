const InputLoader = require('./InputLoader');
const Total = require('./Total');
const Articles = require('./Articles');
const Carts = require('./Carts');
const DeliveryFees = require('./DeliveryFees');

/**
 * Answer module. Create the needed total objet
 * @param {string} path: the path of the input file
 */
module.exports = async function Answer(path) {
  try {
    // load the input file
    const input = await new InputLoader(path, new Articles(), new Carts(), new DeliveryFees());
    // create the total object. It will contain the total cost of each cart
    const total = new Total();
    // process the total cost of each cart
    total.cartsTotal(input.articles, input.carts, input.deliveryFees);

    return total;
  } catch (error) {
    throw error;
  }
};
